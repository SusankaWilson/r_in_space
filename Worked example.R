##############################################################################
# Title: Spatial modelling for cRumpet
# Author: Susan Wilson
# Date: 22 March 2019
##############################################################################
# This tutorial is designed to be worked through in your own time. 
# Everything done here can be done another way (maybe even a better way),but here are some ideas to get you started. 
##############################################################################
# Working with spatial data 
##############################################################################

# add the requisite packages

if (!require(pacman))
  install.packages("pacman")
pacman::p_load(sp,spdep,rgdal,rgeos,raster,rmapshaper,ggmap,geosphere,leaflet,tidyverse,shiny,readxl,PCcharts)

 #sp was the first package to provide classes and methods for spatial data types. It provides classes and methods for creating points, lines, polygons and grids.
# 
# library(raster)  # raster data
# library(rgdal)  # input/output, projections
# library(rgeos)  # geometry ops
# library(spdep)  # spatial dependence
# library(ggmap)
# library(rmapshaper)
# library(geosphere)
# library(leaflet)
# library(PCcharts)
# library(readxl)
# library(methods)
# library(shiny)

##############################################################################
## 1. Spatial points

# A spatial points object...

x <- c(1,2)
y <- c(2,3)

mypoint <- cbind(x,y) %>% data.frame() # Give the point an x and y value


myspatialpoint <- SpatialPoints(mypoint) # turn it into a spatial object

str(myspatialpoint) # Take a look. (Some complex code)

# Attach some data 

mydata <- c("Data about the point.","Data about the other point.") %>% data.frame()

myspatialpointdf <- SpatialPointsDataFrame(myspatialpoint, mydata)
str(myspatialpointdf)


plot(mypoint)

plot(myspatialpointdf)
text(myspatialpointdf@coords, labels=myspatialpointdf@data$.)


## Using ggmap

# Ggmap is good for stationary maps and geocoding. It's not my 'go to', but it's good to know how it works. 
# To get started, you'll need to get yourself up a Google Developor account to get an api key. It's free, but requires billing info. 
# You can find instructions here:https://developers.google.com/maps/documentation/embed/get-api-key

# Once you have your API key you can uncomment then following lines. 

##############
register_google("myAPIkey") 

## The get_map function works a bit like the search function in Google maps - more info is better to ensure you get the right map.

#PC_map <- get_map("Productivity Commission, Melbourne", maptype = "hybrid", zoom = 20)   

PC_map <- readRDS("PC_map.rds") # Here's something I prepared earlier

ggmap(PC_map) #plot the output

mywork <- geocode("Productivity Commission, Melbourne")
mywork <- readRDS("mywork.rds")

ggmap(PC_map)+ geom_label(aes(x=mywork$lon, y= mywork$lat, label = "This is where I work!" ), colour="blue", size=5)

#Australia <- get_map("Australia", zoom = 4)
Australia <- readRDS("Australia.rds")

# I've used geocoding to get airport locations for mapping at work. 

 # airports <- tibble(airportnames = c("Sydney Airport","Melbourne Airport", "Perth Airport",
 #                    "Brisbane Airport","Canberra "))
 # 
 # airports <- airports %>% 
 #   mutate_geocode(airportnames)


airports <- readRDS("airports.rds")
ggmap(Australia) + geom_label(data = airports, aes(label=airportnames))

## Using leaflet 

# I often use the geocode function from ggmaps to get points for locations, but in general I use leaflet for anything where interactivity will be beneficial, including Shiny apps and slides.

# Leaflet works in layers. The call to leaflet creates an empty layer, ready for adding map tiles

leaflet() 

# You can then add tiles - there are lots of different choices

leaflet() %>% addTiles() # base tiles are from OpenStreetMap, but you can add different types as below
leaflet() %>% addProviderTiles(providers$Stamen.Terrain) 
leaflet() %>% addProviderTiles(providers$Stamen.Toner) 
leaflet() %>% addProviderTiles(providers$Esri.NatGeoWorldMap) 

# Then just alter the map as you wish. There are very many options. 

leaflet() %>% 
  addTiles() %>% 
  setView(lat=mywork$lat, lng=mywork$lon, zoom=20) %>% 
  addMarkers(lat=mywork$lat, lng=mywork$lon, label="This is a fully customisable label") #If you don't want to spend time geocoding but you need a place's location, you can find it in the url when you search in google maps. 

# IMPORTANT: Leaflet is lightweight, but adding lots of polygons and markers will weigh it down very quickly making your super fast app or map slow and unusable. 
# (more != better)

##############################################################################
# Mapping antibiotic use
#########################
rm(list=ls()) # Starting with a clean slate

library(readxl)

# Import data for the number of PBS/RPBS prescriptions dispensed for antibiotic medicines per 100,000 children, 9 years and under, by SA3, 2016–17. 
# Data are available here: https://www.safetyandquality.gov.au/atlas/the-third-australian-atlas-of-healthcare-variation-2018/

myfile <- list.files(pattern = ".xlsx")

# Then I need some shape files at the SA3 level. Shape files for statistical areas are available here: http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202016
# I like ESRI shape files. They are easy to use and the files from the ABS often include useful preloaded statistical data, for example, population or size. 

SA3 <- readOGR(dsn=".", "SA3_2016_AUST") # the is from the rgdal package. If you have a particular folder where your maps are saved, you can put the path 

#SA3 <- sf::st_read(dsn=".", "SA3_2016_AUST")
# The result is a Large SpatialPolygonsDataFrame with a data slot and a polygon for each SA3. 

slotNames(SA3)

# The slot includes the three essential elements (points, a grid and a bounding box), data and, because there are multiple polygons, a plot order. 
# You can access a slot using the @ symbol, and then using the $ sign like with any other data frame.

View(SA3@data) #340 rows - one for each SA3
View(SA3@polygons) #340 polygons 
View(SA3@plotOrder) # this slot shows which order the shapes go in. 
View(SA3@bbox) # this is the bounding box for the entire map. You can caluclate the bounding box for any polygon or collection of points using the bbox() function from the sp package.
View(SA3@proj4string) # this slot determines the grid (the coordinate reference system) for the map. 

#You can find plenty of information about CRS, including how to switch between them, here: https://www.nceas.ucsb.edu/~frazier/RSpatialGuides/OverviewCoordinateReferenceSystems.pdf 

# We can plot it very easily. 
plot(SA3)

# The data in each slot are linked: the row names match the ID value for each polygon. I'm using a subsample for now. 

Victoria <- SA3[SA3@data$STE_NAME16=="Victoria",]

plot(Victoria)

## Now we can integrate with leaflet

leaflet(Victoria) %>% 
  addTiles() %>% 
  addPolygons(label = ~SA3_NAME16)

# I can make the labels a bit more fancypants using html wrapper functions. 

labels <- sprintf("<strong>%s</strong><br/>State: %s <br/>Size in SQKM %s ",
                 Victoria$SA3_NAME16, Victoria$STE_NAME16,  Victoria$AREASQKM16) %>%
  lapply(htmltools::HTML)

leaflet(Victoria) %>% 
  addTiles() %>% 
  addPolygons(label = ~labels)

# This map is great at first, but you'll see it mindnumbingly slow. Rm simplify is a useful little function in the mapshaper package. It removes unneccesary detail from polygons, basically by removing points. 
# All the attached data is retained.

detailedSA3 <- SA3 # saving this for later

#SA3 <-rmapshaper::ms_simplify(SA3, keep = 0.01, keep_shapes = T) # this retains 1% of the polygon, which is generally enough if you include 'keep_shapes=T'. 

SA3 <- readRDS("SA3_simplified.rds")
names(SA3@polygons)<- NULL #this step is necessary due to a bug in the ms_simplify package that adds in names of the polygons, confusing the id matching. 
# It's only present in the latest version, so if you have an earlier version (if you added it prior to October 2018) this step is not necessary.

plot(SA3)

##############################################################################
# Attaching the data
##############################################################################

# Reading in the data 

antibiotics <- read_excel("1.2-Data-file-Antibiotic-dispensing-in-children-2016-17.xlsx", sheet = "Scripts (SA3)", 
                          col_types = c("text", "text", "text", "numeric", "text","text", "numeric","numeric","numeric","numeric"),  
                          skip = 1, trim_ws = T) #Sometimes it's easier to hardcode the column types. I could also have done this afterwards. 


antibiotics <- antibiotics %>% 
  mutate(`SA3 code 2016` = `SA3 code 2016` %>% 
           factor(levels = levels(SA3$SA3_CODE16))) # This is to ensure the factor levels of the SA3 data are the same, inlcuding in the same order, as for the AHVdata

newdata <- left_join(SA3@data, antibiotics, c("SA3_CODE16" = "SA3 code 2016"))

SA3@data <- newdata

labels <- sprintf("<h3>%s</h3>
                  Crude rate per 100,000: <strong>%s</strong><br/>
                  Age-sex standardised rate per 100,000: <strong>%s</strong><br/>
                  Decile of age-sex standardised rate: <strong>%s</strong>",
                  SA3$SA3_NAME16, 
                  SA3$`Crude rate per 100,000`, 
                  SA3$`Age-sex standardised rate per 100,000`,
                  SA3$`Decile of age-sex standardised rate`) %>%
  lapply(htmltools::HTML) 

# htmltools is lots of fun. There are three main components to the In the above code: the sprintf function, lapply function and the HTML wrapper. 
# The sprintf function map the values from the data to the 

leaflet(SA3) %>% 
  addTiles() %>% 
  addPolygons(weight = 1, label = labels)


pal <- colorFactor(palette = "YlOrRd", 
                   domain = SA3$`Decile of age-sex standardised rate`,
                   na.color = NA) #this creates a colour palette that is function of your variable
# I've set the na colour to NA, which effectively removes NAs form the colour mapping. When working 
# with a regular data frame, I'd just remove the rows with NAs before plotting, or deal with them as appropriate. 
# In the case of spatial data, however, working with 


leaflet(SA3) %>% 
  addTiles() %>% 
  addPolygons(color = "grey", 
              weight = 1, 
              fillColor = ~pal(`Decile of age-sex standardised rate`), 
              label = labels,
              opacity = 0.8) %>% 
  addLegend(pal = pal, 
            values =~`Decile of age-sex standardised rate`,
            position = "bottomleft",
            title = "<p>
            Age-sex standardised rate</p>
            <p>(decile)</p>")


##############################################################################
# Extensions
##############################################################################

## Static maps with ggplot and fortify

#Leaflet is great for interactive maps, but sometimes a static map is better. We can use ggplot for that. 

SA3_fortified <- fortify(SA3) #this converts a Spatial Polygons Dataframe to a regular dataframe. The data slot is not included in the resulting dataframe, but it's straightforward to add 

SA3@data <- SA3@data %>%     #This is to prepare the data slot for merging with another dataframe, by id. 
  mutate(id = row.names(.))  #I'm trying to be at one with the tidyverse, otherwise I would do it like this: SA3@data$id<-row.names(SA3@data) 

SA3_fortified <- left_join(SA3_fortified, SA3@data, c("id" = "id")) #Standard merge of two dataframes.


ggplot(SA3_fortified, aes(long,lat, group=group))+  # This follows regular ggplot processes
  geom_polygon(aes(fill=`Decile of age-sex standardised rate`))+
  coord_fixed(1.1)+
  theme_void()+
  labs(title = "")+
  theme(legend.position = "bottom")

## Distances between points

# Let's say I want to know the distance between two points. 

df<-data.frame("x"=c(1,2),
               "y"=c(2,3))

ggplot(df, aes(x,y))+geom_point(size=3)

# First, we can measure the Euclidean distance

point1<-data.matrix(df[1,])
point2<-data.matrix(df[2,])

spDistsN1(point1, point2) # Pythagoras says this is the right answer.

points1<- data.matrix(df) # It works with multiple points too. The first entry is the matrix of points, and the second is a single point from which the distance is calculated.
point3 <- matrix(c(1,1), nrow=1) 

spDistsN1(points1, point3)

# I can do the same things using their lat/lon positions. 

# You need an API key for the below. If you have added one, just remove the comments and delete the data I prepared earlier. 

#CanberraOffice <- geocode("Productivity Commission, Canberra") %>% as.matrix()
CanberraOffice <- readRDS("CanberraOffice.rds") # Here's something I prepared earlier

#MelbourneOffice<-geocode("Productivity Commission, Melbourne") %>% as.matrix()
MelbourneOffice <- readRDS("MelbourneOffice.rds")  # Here's something I prepared earlier

spDistsN1(MelbourneOffice, CanberraOffice) # spDistN1 gives a value in the same units provided in the data. Not as useful for lat/lon
spDistsN1(MelbourneOffice, CanberraOffice, longlat = T) # When longlat=T, the value is reported in kilometres. 

## Great circle paths

# Finally, before I set you to work making a leaflet map, I'm going to show you something fun. 


citypairs <- read_excel("International_airline_activity_OpFltsSeats_1118_Tables.xlsx", 
                        sheet = "CityPairs", 
                        trim_ws = T,
                        skip = 10,
                        col_names = T)

citypairs <- citypairs %>% 
  dplyr::select(Month, Australian, International) %>% 
  mutate(Year = Month %>% 
           as.Date() %>% format("%Y"),
         Australian = paste(Australian, "Airport"),
         International=paste(International, "Airport"))

# getting a list of the unique locations for geocoding, and ensuring I'm geocoding the Australian location
# NB I could also geocode the whole of citypairs, but it's 24233 observations, so it would take ages and use up all my geocoding credits. 

Auslocations <- tibble(AustralianAirports = paste(unique(citypairs$Australian), ", Australia", sep = ""))  
Intlocations <- tibble(InternationalAirports = unique(citypairs$International))

# Auslocations <- Auslocations %>% 
#   mutate_geocode(AustralianAirports) %>% 
#   mutate(lonA = lon, # Giving the lat and lon a unique name, ready for later merging
#          latA = lat,
#          AusAirports=AustralianAirports %>% str_replace(", Australia", "") # Creating a new column with the name of the airports - note that this only works because the geocoding had no failed locations.
#          ) %>% dplyr::select(AusAirports, lonA, latA) 

Auslocations <- readRDS("Auslocations.rds")  # Here's something I prepared earlier

# Intlocations <- Intlocations %>% 
#   mutate_geocode(InternationalAirports) %>% 
#   mutate(lonI=lon, # Giving the lat and lon a unique name, ready for later merging
#          latI=lat,
#          IntAirports = InternationalAirports) %>%  # Creating a new column with the name of the airports
#   dplyr::select(IntAirports, lonI, latI)
#I added the dplyr:: in front of the 'select' function because one of the other packages that I have loaded has a different select function that conflicts with the one I'm using. This ensures that R knows which select() I'm after.




# Intlocations<-geocode(InternationalAirports) %>% 
#   mutate(lonI=lon, # Giving the lat and lon a unique name, ready for later merging
#          latI=lat,
#          IntAirports = InternationalAirports) %>%  # Creating a new column with the name of the airports 
#   dplyr::select(IntAirports, lonI, latI)

Intlocations <- readRDS("Intlocations.rds")

citypairs<-citypairs %>% 
  left_join(Auslocations, 
            c("Australian"="AusAirports")) %>% 
  left_join(Intlocations, c("International" = "IntAirports")) %>% 
  dplyr::select(Year, Australian, lonA, latA, International, lonI, latI) %>% 
  unique() #We've grouped by year, rather than month, but since we are not using any of the numbers here, there are lots of duplicated rows

p1 <- data.matrix(citypairs[,c("lonA","latA")]) # the origin point (as a matrix)
p2 <- data.matrix(citypairs[,c("lonI","latI")]) # The destination point (as a matrix)

flightpaths  <-gcIntermediate(p1, p2, breakAtDateLine=T, # This is probably my favourite functions in R (in the geosphere package). It maps the great circle path between two points, so you can plot the curvature of the earth.
                                     n=1000, # this number can be changed. It's the number of points that are plotted to make up the path. As usual, more detail = slower
                                     addStartEnd=TRUE,
                                     sp=TRUE)


leaflet() %>% 
  addTiles() %>% 
  addCircleMarkers(Auslocations, lng = Auslocations$lonA, lat = Auslocations$latA, radius = 1, label = Auslocations$AusAirports)%>%
  addCircleMarkers(Intlocations, lng = Intlocations$lonI, lat = Intlocations$latI, radius = 1, label = Intlocations$IntAirports, color = "#265a9a") %>%
  addPolylines(data = flightpaths, weight = 1, color = "#265a9a")  

# fun, but SLOW and kind of useless, really. Remember when I said less was more? We can subset using a shiny app. 

saveRDS(citypairs, "citypairs.rds") # The data needs to be in a standalone file for the Shiny App

##########################################################
# Making it useful. 

citypairs<-readRDS("citypairs.rds")

ui <- fluidPage(tags$h2("Great circle flight paths"),

    fluidRow(column(12,
                    selectInput("airport", "Select an airport", choices = unique(citypairs$Australian)))),
    fluidRow(column(12,
                    leafletOutput("mymap", 
                                  width = "100%",
                                  height=600)))
)

# Define server logic required to draw a histogram
server <- function(input, output) {
  
 flightpaths<- reactive({ 
   citypairs<-citypairs %>% 
     filter(Australian==input$airport)
  
   p1 <- data.matrix(citypairs[,c("lonA","latA")]) # the origin point (as a matrix)
   p2 <- data.matrix(citypairs[,c("lonI","latI")]) # The destination point (as a matrix)
   
   df <-gcIntermediate(p1, p2, breakAtDateLine=T, # This is probably my favourite functions in R (in the geosphere package). It maps the great circle path between two points, so you can plot the curvature of the earth.
                                 n=1000, # this number can be changed. It's the number of points that are plotted to make up the path. As usual, more detail = slower
                                 addStartEnd=TRUE,
                                 sp=TRUE)
   
   
   return(df)
 })
 
 output$mymap <- renderLeaflet(leaflet() %>% 
                                 addTiles() %>% 
                               setView(lng=130,
                                       lat=-12,
                                        zoom=3) %>% 
                                 addMarkers(lng = unique(citypairs$lonA),
                                            lat = unique(citypairs$latA),
                                            label = unique(citypairs$Australian)))
 
 observe(leafletProxy("mymap")%>%
           clearShapes() %>% 
           clearMarkers() %>% 
           addPolylines(data = flightpaths(),weight = 1, color = "#265a9a")) 
 
}


# Run the application 
shinyApp(ui = ui, server = server)

